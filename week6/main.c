#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
    int i;
    static int size;
    printf("Enter the size of array:");
    scanf("%d",&size);
    int arr[size];
    printf("Enter the array elements:");
    for(i = 0;i<size;i++){
        scanf("%d",&arr[i]);
    }
    printf("Before the sort:\n");
    printArray(arr,size);
    printf("\n After the sort:");
    stoogeSort(arr,0,size-1);
    printArray(arr,size);
    return 0;
}

void stoogeSort(int arr[], const int low, const int high){

if(low>=high){
    return;
}if(arr[low]>arr[high]){
    swap(&arr[low],&arr[high]);
}
if(high-low+1>2){
    int size = high-low+1;
    stoogeSort(arr,low,high-floor(size/3));
    stoogeSort(arr,low+floor(size/3),high);
    stoogeSort(arr,low,high-floor(size/3));

}

}

void swap(int* p, int* k){
    int temp;
    temp = *p;
    *p = *k;
    *k = temp;
}

void printArray(const int arr[],const int size){
    int i ;
    for (i=0;i<size;i++){
        printf("\n%d. index= %d",i,arr[i]);
    }

}


