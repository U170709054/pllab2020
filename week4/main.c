#include <stdio.h>
#include <stdlib.h>
#include <math.h>

FILE *outp;

float arb_funtion(float x)
{
    return x*log10(x) - 1.2;
}

void rf(float *x, float x0, float x1, float fx0, float fx1, int *itr )
{
    *x = ((x0*fx1)-(x1 * fx0)) / (fx1 - fx0);
    ++(*itr);
    printf("Iteration %d: %.5f\n", *itr,*x);
    fprintf(outp,"Iteration %d: %.4f\n", *itr,*x);
}

int main()
{

    int itr,maxitr;
    float x,x0,x1,x_curr,x_next,error,fx0,fx1;

    outp = fopen("rf.txt", "w");

    printf("Enter x0,x1,error,maxitr values: ");
    scanf("%f %f %f %d",&x0,&x1,&error,&maxitr);
    rf(&x_curr,x0,x1,arb_funtion(x0),arb_funtion(x1),&itr);

    do{
        if(arb_funtion(x0) * arb_funtion(x_curr) < 0){
            x1 = x_curr;
        }
            else{
                x0 = x_curr;
            }

        rf(&x_next,x0,x1,arb_funtion(x0),arb_funtion(x1),&itr);

        if(fabs(x_next - x_curr) < error){
            printf("After %d try, you reached the root: %.4f",itr,x_curr);
            fprintf(outp,"After %d try, you reached the root: %.4f \n", itr, x_curr);
            return 0;
        }else{
            x_curr = x_next;
        }
    }while(itr < maxitr);


    fclose(outp);
    return 0;
}


